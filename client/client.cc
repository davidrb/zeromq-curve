#include <czmq.h>
#include <iostream>
#include <memory>
#include <tuple>

#include "../common/czmq_raii.h"

int main () {
    auto cert = ZCert{};
    auto client = ZSock{ZMQ_REQ};

    zcert_save_public (cert, "curve/client.pub");

    zcert_apply(cert, client);

    auto server_cert = ZCert{zcert_load("curve/server.pub")};
    auto public_key = zcert_public_txt(server_cert);

    std::cout << "public key: " << public_key << std::endl;

    zsock_set_curve_serverkey(client, public_key);
    zsock_connect(client, "tcp://127.0.0.1:9000");

    std::cout << "sending Hello" << std::endl;
    zstr_send (client, "Hello");

    auto message = ZStr{zstr_recv(client)};
    std::cout << "received: " << message << std::endl;

    return 0;
}
