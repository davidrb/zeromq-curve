# Building
- Install bazel
- run bazel build server client

# Running 
- ./bazel-bin/server/server
- ./bazel-bin/client/client
