#include <czmq.h>
#include <iostream>
#include <memory>

#include "../common/czmq_raii.h"

int main () {
    //auto auth = zactor_new(zauth, NULL);
    auto auth = ZActor{zauth, nullptr};

    zstr_send(auth, "VERBOSE");
    zsock_wait(auth);

    zstr_sendx(auth, "CURVE", "./curve", NULL);
    zsock_wait(auth);

    auto cert = ZCert{zcert_load("curve/server.pub")};
    if (!cert) {
        std::cout << "generating new certificate" << std::endl;
        zsys_dir_create("./curve");
        cert = ZCert{};
        zcert_set_meta(cert, "name", "server");
        zcert_save(cert, "curve/server.pub");
    }

    auto public_key = zcert_public_txt(cert);

    std::cout << "public key: " << public_key << std::endl;

    auto server = ZSock{ZMQ_REP};
    zsock_set_zap_domain(server, "global");
    zcert_apply(cert, server);
    zsock_set_curve_server(server, 1);
    zsock_bind(server, "tcp://127.0.0.1:9000");

    auto message = ZStr{zstr_recv(server)};
    std::cout << message << std::endl;

    zstr_send (server, "Hello");

    return 0;
}
