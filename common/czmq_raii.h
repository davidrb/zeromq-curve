#ifndef CZMQ_RAII_H
#define CZMQ_RAII_H

template<typename T, void dfn(T**)>
struct czmq_deleter {
  void operator()(T *p){ dfn(&p); }
};

template<typename T, typename CT, CT cfn, typename DT, DT dfn>
struct czmq_obj {
  private:
    std::unique_ptr<T, czmq_deleter<T, dfn>> ptr;
  public:
    czmq_obj(czmq_obj const&) = delete;
    czmq_obj(czmq_obj&& r) { ptr = std::move(r.ptr); };
    czmq_obj(T* p) : ptr{p} {}

    template<typename... Args>
      czmq_obj(Args... args) : ptr{ cfn(args...) } {}


    auto operator=(T* p) -> czmq_obj& { ptr = {p}; return *this; }
    auto operator=(czmq_obj&&) -> czmq_obj& = default;

    operator T* () { return ptr.get(); };
};

using ZCert = czmq_obj<zcert_t,
      decltype(&zcert_new), zcert_new,
      decltype(&zcert_destroy), zcert_destroy>;

using ZSock = czmq_obj<zsock_t,
      decltype(&zsock_new), zsock_new,
      decltype(&zsock_destroy), zsock_destroy>;

using ZStr = czmq_obj<char,
      decltype(&zstr_recv), zstr_recv,
      decltype(&zstr_free), zstr_free>;

using ZActor = czmq_obj<zactor_t,
      decltype(&zactor_new), zactor_new,
      decltype(&zactor_destroy), zactor_destroy>;

#endif
